"""HTTP(S) endpoints."""
from cki_lib import misc
from cki_lib.logger import get_logger
import flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from .queue import IRC_PUBLISH_QUEUE

app = flask.Flask(__name__)

misc.sentry_init(sentry_sdk, integrations=[FlaskIntegration()], environment=app.env)

LOGGER = get_logger(__name__)


@app.route('/', methods=['GET'])
def index():
    """Return a nearly empty page."""
    return flask.Response(response='OK', status='200')


@app.route('/message', methods=['POST'])
def message_receiver():
    """Route a generic message to IRC."""
    data = flask.request.get_json()
    message = data['message']
    IRC_PUBLISH_QUEUE.put(message)
    return flask.Response(response='OK', status='200')


@app.route('/sentry', methods=['POST'])
def sentry_receiver():
    """Route a Sentry message to IRC."""
    data = flask.request.get_json()
    project_name = data['project_name']
    message = data['message']
    url = misc.shorten_url(data['url'])
    message = f'😩 {project_name}: {message} - {url}'
    IRC_PUBLISH_QUEUE.put(message)
    return flask.Response(response='OK', status='200')


@app.route('/grafana', methods=['POST'])
def grafana_alert_receiver():
    """Route a Grafana alert to IRC."""
    data = flask.request.get_json()
    title = data['title']
    emoji = '🔵' if 'OK' in title else '🔴'
    values = ', '.join(
        f'{item["metric"]}: {item["value"]}'
        for item in data.get('evalMatches', [])
    )
    message = f'{emoji} {title} - {values}'
    IRC_PUBLISH_QUEUE.put(message)
    return flask.Response(response='OK', status='200')


def _alert(alerts, alertmanager_url, status):
    filtered = [a for a in alerts if a['status'] == status]
    if filtered:
        alert = filtered[0]
        emoji = '🔵' if alert['status'] == 'resolved' else '🔴'
        what = alert['status'].upper()
        if len(filtered) > 1:
            what += f':{len(filtered)}'
        alert_name = alert['labels']['alertname']
        summary = alert['annotations']['summary']
        dashboard = alert['annotations'].get('dashboard', '')
        message = f'{emoji} [{what}] {alert_name}: {summary}'
        if dashboard:
            message += f' - dashboard {misc.shorten_url(dashboard)}'
        if alert['status'] == 'firing':
            message += f' - alertmanager {alertmanager_url}'

        IRC_PUBLISH_QUEUE.put(message)


@app.route('/alertmanager', methods=['POST'])
def alertmanager_receiver():
    """Route an alertmanager message to IRC."""
    data = flask.request.get_json()
    alertmanager_url = misc.shorten_url(data['externalURL'])
    LOGGER.info('alertmanager %s', data)

    _alert(data['alerts'], alertmanager_url, 'resolved')
    _alert(data['alerts'], alertmanager_url, 'firing')

    return flask.Response(response='OK', status='200')
