import unittest
from unittest import mock

from cki_lib.irc_message import style
import gitlab
import responses

from irc_bot import irc_commands
from irc_bot.gitlab import Epic
from irc_bot.gitlab import Issue
from irc_bot.gitlab import MergeRequest


class TestGitlabLink(unittest.TestCase):
    """Test GitlabLink methods."""

    def setUp(self):
        """Set up GitLabLink objects."""
        self.gitlab_link = irc_commands.GitLabLink()

    def test_mr_patterns(self):
        """Test patterns are correctly recognized."""
        event = mock.Mock()
        process = mock.Mock()
        self.gitlab_link.process_merge_request = process

        cases = [
            ('https://gitlab.com/cki-project/foo/-/merge_requests/253',
             {'host': 'gitlab.com', 'project': 'cki-project/foo', 'iid': '253'}),
            ('gitlab.com/cki-project/foo/-/merge_requests/253',
             {'host': 'gitlab.com', 'project': 'cki-project/foo', 'iid': '253'}),
            ('cki-project/foo!123',
             {'host': None, 'project': 'cki-project/foo', 'iid': '123'}),
        ]

        for url, parameters in cases:
            event.arguments = [url]
            self.gitlab_link(None, event)
            process.assert_called_with(
                None, event,
                host=parameters['host'],
                project=parameters['project'],
                iid=parameters['iid']
            )
            process.reset_mock()

    def test_issue_patterns(self):
        """Test patterns are correctly recognized."""
        event = mock.Mock()
        process = mock.Mock()
        self.gitlab_link.process_issue = process

        cases = [
            ('https://gitlab.com/cki-project/foo/-/issues/253',
             {'host': 'gitlab.com', 'project': 'cki-project/foo', 'iid': '253'}),
            ('gitlab.com/cki-project/foo/-/issues/253',
             {'host': 'gitlab.com', 'project': 'cki-project/foo', 'iid': '253'}),
            ('cki-project/foo#123',
             {'host': None, 'project': 'cki-project/foo', 'iid': '123'}),
        ]

        for url, parameters in cases:
            event.arguments = [url]
            self.gitlab_link(None, event)
            process.assert_called_with(
                None, event,
                host=parameters['host'],
                project=parameters['project'],
                iid=parameters['iid']
            )
            process.reset_mock()

    def test_epic_patterns(self):
        """Test patterns are correctly recognized."""
        event = mock.Mock()
        process = mock.Mock()
        self.gitlab_link.process_epic = process

        cases = [
            ('https://gitlab.com/groups/cki-project/-/epics/1',
             {'host': 'gitlab.com', 'project': 'cki-project', 'iid': '1'}),
            ('gitlab.com/groups/cki-project/-/epics/1',
             {'host': 'gitlab.com', 'project': 'cki-project', 'iid': '1'}),
            ('cki-project&2',
             {'host': None, 'project': 'cki-project', 'iid': '2'}),
        ]

        for url, parameters in cases:
            event.arguments = [url]
            self.gitlab_link(None, event)
            process.assert_called_with(
                None, event,
                host=parameters['host'],
                project=parameters['project'],
                iid=parameters['iid']
            )
            process.reset_mock()

    def test_get_pattern_iterator(self):
        """Test _get_patterns_iterator."""
        self.gitlab_link.patterns = ['pattern_1_{}', 'pattern_2_{}']
        self.gitlab_link.objects = {'obj_1': ['id_1_1', 'id_1_2'],
                                    'obj_2': ['id_2_1', 'id_2_2']}

        all_patterns = list(self.gitlab_link._get_patterns_iterator())
        self.assertEqual(
            [
                ('obj_1', 'pattern_1_id_1_1'),
                ('obj_1', 'pattern_2_id_1_1'),
                ('obj_1', 'pattern_1_id_1_2'),
                ('obj_1', 'pattern_2_id_1_2'),
                ('obj_2', 'pattern_1_id_2_1'),
                ('obj_2', 'pattern_2_id_2_1'),
                ('obj_2', 'pattern_1_id_2_2'),
                ('obj_2', 'pattern_2_id_2_2')
            ],
            all_patterns
        )


class TestGitlabObjects(unittest.TestCase):
    """Test Gitlab messages."""

    @staticmethod
    def mock_requests():
        """Mock Gitlab requests."""
        mocks = [
            ('https://foo/api/v4/projects/1',
             {'id': 1, 'path': 'proj/path'}),
            ('https://foo/api/v4/projects/1/merge_requests/1',
             {'iid': 1, 'author': {'username': 'authorname'}, 'state': 'merged',
              'title': 'titlefoobar',
              'web_url': 'https://foo/mr/url'}),
            ('https://foo/api/v4/projects/1/merge_requests/1/'
             'changes?access_raw_diffs=True',
             {'changes_count': "1", 'changes': [{'diff': '-a\n+b\n+c'}]}),
            ('https://foo/api/v4/projects/1/merge_requests/1/pipelines?per_page=1',
             [{'status': 'success'}]),
            ('https://foo/api/v4/projects/1/issues/1',
             {'iid': 1, 'author': {'username': 'authorname'}, 'state': 'opened',
              'title': 'titlefoo', 'web_url': 'https://foo/issue/url'}),
            ('https://foo/api/v4/groups/1',
             {'id': 1, 'path': 'group/path'}),
            ('https://foo/api/v4/groups/1/epics/1',
             {'iid': 1, 'author': {'username': 'authorname'}, 'state': 'opened',
              'title': 'titlefoo', 'web_url': 'https://foo/issue/url'}),
        ]

        for url, json in mocks:
            responses.add(responses.GET, url, json=json)

    @responses.activate
    def setUp(self):
        """Set up."""
        self.mock_requests()
        self.gitlab_client = gitlab.Gitlab('https://foo', 'bar')
        self.project = self.gitlab_client.projects.get(1)
        self.group = self.gitlab_client.groups.get(1)

    @responses.activate
    def test_merge_request(self):
        """Test MergeRequest message."""
        self.mock_requests()
        mr = MergeRequest(
            self.project,
            self.project.mergerequests.get(1)
        )

        self.assertEqual(
            f'{style("proj/path!1", bold=True)} authorname '
            f'1/{style("+2", fg="lime")}/{style("-1", fg="red")} '
            f'[MERGED/{style("PASS", fg="lime")}] titlefoobar - https://foo/mr/url',
            str(mr)
        )

    @responses.activate
    def test_issue(self):
        """Test Issue message."""
        self.mock_requests()
        issue = Issue(
            self.project,
            self.project.issues.get(1)
        )

        self.assertEqual(
            f'{style("proj/path#1", bold=True)} authorname '
            f'[OPENED] titlefoo - https://foo/issue/url',
            str(issue)
        )

    @responses.activate
    def test_epic(self):
        """Test Epic message."""
        self.mock_requests()
        epic = Epic(
            self.group,
            self.group.epics.get(1)
        )

        self.assertEqual(
            f'{style("group/path&1", bold=True)} authorname '
            f'[OPENED] titlefoo - https://foo/issue/url',
            str(epic)
        )
